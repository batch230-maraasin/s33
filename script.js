//console.log("Hello World");
// -------------------------// 3.

fetch("https://jsonplaceholder.typicode.com/todos")
.then(res => res.json()) 



// -----------------------------// 4.
fetch('https://jsonplaceholder.typicode.com/todos')
.then((response) => response.json())
.then((json) => {

	let title = json.map((todos => {
		return todos.title;
	}))

	console.log(title);

});

// -------------------------------// 5.
fetch("https://jsonplaceholder.typicode.com/todos/1")
.then(res => res.json()) 
.then(response => console.log(response))

// --------------------------------// 6.

fetch("https://jsonplaceholder.typicode.com/todos/1")
.then(res => res.json()) 
.then(response => console.log("The item " + response.title + " on the list has a status of " + response.completed))

// ----------------------------------// 7.

fetch("https://jsonplaceholder.typicode.com/todos", 
	{
		method: "POST",
		headers: {
			"Content-Type" : "application/json"
		},
		body: JSON.stringify({
			completed: "false",
			title: "Created To Do List Item",
			userId:1
		})
	})
	.then(response => response.json())
	.then(json => console.log(json))

// -----------------------------------// 8 and 9

fetch("https://jsonplaceholder.typicode.com/todos/1", 
	{
		method: "PUT",
		headers: {
			"Content-Type" : "application/json"
		},
		body: JSON.stringify({
			dateCompleted: "Pending",
			description: "To update the my to do list with a different data structure.",
			status: "Pending",
			title: "Updated To Do List Item",
			userId:1
		})
	})
	.then(response => response.json())
	.then(json => console.log(json))

// -----------------------------------// 10 and 11

fetch("https://jsonplaceholder.typicode.com/todos/1", 
	{
		method: "PATCH",
		headers: {
			"Content-Type" : "application/json"
		},
		body: JSON.stringify({
			dateCompleted: "07/09/21",
			status: "Complete",
		})
	})
	.then(response => response.json())
	.then(json => console.log(json))

//-------------------------------------// 12

fetch("https://jsonplaceholder.typicode.com/todos/1", 
	{
		method: "DELETE"
	}	
).then(response => response.json())
.then(json => console.log(json))






